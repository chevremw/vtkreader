// Everything here is only for python wrapping.
#include <boost/multi_array.hpp>
#include <boost/foreach.hpp>
#include <boost/python.hpp>

#include "Reader.h"


namespace py=boost::python;
using namespace VtkReader;


BOOST_PYTHON_MODULE(VtkReader)
{
    py::class_<Reader>("Reader", py::init<std::string>())
        .def("load", &Reader::load)
        .def("pointData", &Reader::getPointData)
        .def("volumeMatrix", &Reader::getVolumeMatrix, (py::arg("from"), py::arg("to"), py::arg("N")=200, py::arg("mask")=0, py::arg("dir")=1))
        .add_property("pointDataNames", &Reader::getDataNames)
        .add_property("pos", &Reader::getPositions);
}

