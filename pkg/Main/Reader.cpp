#include "Reader.h"

using namespace VtkReader;

Reader::Reader(std::string filename) : 
    m_filename(filename)
{
}

Reader::~Reader()
{
    
}

Matrix Reader::getPositions() const
{    
    if(!m_dataSet.get()) return Matrix();
    
    int N(m_dataSet->GetNumberOfPoints());
    Matrix m(boost::extents[N][3]);
    
    double pos[3];
    double* _m = m.data();
    
    for(int i(0);i<N;i++) {
        m_dataSet->GetPoint(i,pos);
        *(_m+3*i+0) = pos[0];
        *(_m+3*i+1) = pos[1];
        *(_m+3*i+2) = pos[2];
    }
    
    return m;
}

Matrix Reader::getPointData(std::string const& title) const
{
    if(!m_dataSet.get()) return Matrix();
    
    vtkPointData *pd = m_dataSet->GetPointData();
    if(!pd) return Matrix();
    
    int n(pd->GetNumberOfArrays());
    int i(0);
    for(;i<n;i++) {
        if(pd->GetArrayName(i) == title) { break; }
    }
    
    if(i==n) return Matrix();
    vtkAbstractArray* d = pd->GetAbstractArray(i);
    
    if(!d || d->GetDataType() != VTK_DOUBLE) { return Matrix(); }
    
    int tuples(d->GetNumberOfTuples());
    int components(d->GetNumberOfComponents());
    
    Matrix m(boost::extents[tuples][components]);
    double *_m = m.data();
    double *vtkDat = (double*)d->GetVoidPointer(0);
    
    std::copy(vtkDat, vtkDat+tuples*components, _m);
    
    return m;
}

Matrix Reader::getVolumeMatrix(double from, double to, int N, int M, int dir) const
{
    if(!m_dataSet.get()) return Matrix();
    
    Matrix pos(getPositions());
    int Np(m_dataSet->GetNumberOfPoints());
    Matrix ret(boost::extents[N][Np]);
    Matrix mask(getPointData("mask"));
    Matrix radii(getPointData("radii"));
    
    double dx((to - from) / ((double)N));
    
    for(int i(0);i<Np;i++) {
        
        if(M > 0 && M != mask[i][0]) continue; // Skip particles that don't matck the mask
        
        double p(pos[i][dir]);
        double r(radii[i][0]);
        
        int minid = floor((p - r - from)/dx)-1;
        int maxid = ceil((p + r - from)/dx)+1;
        
        if(minid < 0) minid = 0;
        if(minid >= N) minid = N-1;
        if(maxid < 0) maxid = 0;
        if(maxid >= N) maxid = N-1;
        
        for(int j(minid);j<=maxid;j++) {
        //for(int j(0);j<N;j++) {
            double sbot(from + ((double)j)*dx);
            double stop(from + ((double)(j+1))*dx);
            
            if( p + r < sbot || p - r > stop) continue; // Check if sphere is effectively inside section
                
            double dh_t = (p + r) - stop;
            double dh_l = sbot - (p - r);
            
            if(dh_t < 0) dh_t = 0; // If not outside
            if(dh_l < 0) dh_l = 0; // ..
            
            ret[j][i] = M_PI/3.*(4.*r*r*r - dh_t*dh_t*(3.*r-dh_t) - dh_l*dh_l*(3.*r-dh_l)); // Total sphere - spherical cap outside
        }
    }
    
    return ret;
}

vector<std::string> Reader::getDataNames() const
{
    vector<std::string> ret;
    
    if(!m_dataSet.get()) return ret;
    
    vtkPointData *pd = m_dataSet->GetPointData();
    if(!pd) return ret;
    
    ret.reserve(pd->GetNumberOfArrays());
    
    for(int i(0);i<pd->GetNumberOfArrays();i++) {
        ret.push_back(pd->GetArrayName(i));
    }
    
    return ret;
}

bool Reader::load()
{
    vtkDataSet *dataSet;
    std::string extension =
      vtksys::SystemTools::GetFilenameLastExtension(m_filename.c_str());
    // Dispatch based on the file extension
    if (extension == ".vtu")
    {
        dataSet = ReadAnXMLFile<vtkXMLUnstructuredGridReader> (m_filename.c_str());
    }
    else if (extension == ".vtp")
    {
        dataSet = ReadAnXMLFile<vtkXMLPolyDataReader> (m_filename.c_str());
    }
    else if (extension == ".vts")
    {
        dataSet = ReadAnXMLFile<vtkXMLStructuredGridReader> (m_filename.c_str());
    }
    else if (extension == ".vtr")
    {
        dataSet = ReadAnXMLFile<vtkXMLRectilinearGridReader> (m_filename.c_str());
    }
    else if (extension == ".vti")
    {
        dataSet = ReadAnXMLFile<vtkXMLImageDataReader> (m_filename.c_str());
    }
    else if (extension == ".vto")
    {
        dataSet = ReadAnXMLFile<vtkXMLHyperOctreeReader> (m_filename.c_str());
    }
    else if (extension == ".vtk")
    {
        dataSet = ReadAnXMLFile<vtkDataSetReader> (m_filename.c_str());
    }
    else
    {
        return false;
    }
    
    m_dataSet = shared_ptr<vtkDataSet>(dataSet, [](vtkDataSet* ds){ds->Delete(); });
    
    return true;
}


