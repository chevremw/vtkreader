
#pragma once


#include <string>

#include <boost/container/vector.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/multi_array.hpp>

#include <vtkSmartPointer.h>
#include <vtkXMLReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLHyperOctreeReader.h>
#include <vtkXMLCompositeDataReader.h>
#include <vtkXMLStructuredGridReader.h>
#include <vtkXMLImageDataReader.h>
#include <vtkDataSetReader.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkRectilinearGrid.h>
#include <vtkHyperOctree.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFieldData.h>
#include <vtkCellTypes.h>
#include <vtksys/SystemTools.hxx>

template<class T>
using shared_ptr=boost::shared_ptr<T>;

template<class T>
using vector=boost::container::vector<T>;

namespace py=boost::python;
namespace np=boost::python::numpy;
using Matrix=boost::multi_array<double, 2>;

namespace VtkReader {
 
class Reader {
public:
    Reader(std::string filename);
    virtual ~Reader();
    
    bool load();
    
    Matrix getPositions() const;
    Matrix getPointData(std::string const&) const;
    vector<std::string> getDataNames() const;
    Matrix getVolumeMatrix(double from, double to, int N, int mask=0, int dir=1) const;
    
protected:
    template<class TReader>
    vtkDataSet *ReadAnXMLFile(const char*fileName)
    {
        vtkSmartPointer<TReader> reader = vtkSmartPointer<TReader>::New();
        reader->SetFileName(fileName);
        reader->Update();
        reader->GetOutput()->Register(reader);
        return vtkDataSet::SafeDownCast(reader->GetOutput());
    }
    
    std::string m_filename;
    shared_ptr<vtkDataSet> m_dataSet;
};
    
}

