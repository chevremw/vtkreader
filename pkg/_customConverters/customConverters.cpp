//
//  python_multi_array.cpp
//  python-multi-array
//
//  Copyright (C) 2017 Rue Yokaze
//  Distributed under the MIT License.
//

#include <boost/container/vector.hpp>
#include <boost/multi_array.hpp>
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <boost/foreach.hpp>
#include <memory>
#include <stdint.h>
#include <vector>
#include <iostream>



//  Using from python is avoided because many definitions conflict with names from std.
using boost::extents;
using boost::multi_array;
using boost::shared_ptr;
namespace np=boost::python::numpy;
namespace python=boost::python;


#define VECTOR_SEQ_CONV(Type) custom_vector_from_seq<Type>();  boost::python::to_python_converter<boost::container::vector<Type>, custom_vector_to_list<Type> >();
#define FOREACH BOOST_FOREACH

/*** c++-list to python-list */
template<typename containedType>
struct custom_vector_to_list{
	static PyObject* convert(const boost::container::vector<containedType>& v){
		boost::python::list ret; FOREACH(const containedType& e, v) ret.append(e);
		return boost::python::incref(ret.ptr());
	}
};
template<typename containedType>
struct custom_vector_from_seq{
	custom_vector_from_seq(){ boost::python::converter::registry::push_back(&convertible,&construct,boost::python::type_id<boost::container::vector<containedType> >()); }
	static void* convertible(PyObject* obj_ptr){
		// the second condition is important, for some reason otherwise there were attempted conversions of Body to list which failed afterwards.
		if(!PySequence_Check(obj_ptr) || !PyObject_HasAttrString(obj_ptr,"__len__")) return 0;
		return obj_ptr;
	}
	static void construct(PyObject* obj_ptr, boost::python::converter::rvalue_from_python_stage1_data* data){
		 void* storage=((boost::python::converter::rvalue_from_python_storage<boost::container::vector<containedType> >*)(data))->storage.bytes;
		 new (storage) std::vector<containedType>();
		 boost::container::vector<containedType>* v=(boost::container::vector<containedType>*)(storage);
		 int l=PySequence_Size(obj_ptr); if(l<0) abort(); /*std::cerr<<"l="<<l<<"; "<<typeid(containedType).name()<<std::endl;*/ v->reserve(l); for(int i=0; i<l; i++) { v->push_back(boost::python::extract<containedType>(PySequence_GetItem(obj_ptr,i))); }
		 data->convertible=storage;
	}
};


template <class T, size_t N>
np::ndarray get(const multi_array<T, N>& This)
{
    size_t s[N];
    size_t d[N];
    std::copy(This.shape(), This.shape() + N, s);
    std::transform(This.strides(), This.strides() + N, d, [](auto input) { return input * sizeof(T); });
    auto make_tuple_from_array = [](const size_t* a) {
        switch (N)
        {
            case 1:
                return python::make_tuple(a[0]);
            case 2:
                return python::make_tuple(a[0], a[1]);
            case 3:
                return python::make_tuple(a[0], a[1], a[2]);
            case 4:
                return python::make_tuple(a[0], a[1], a[2], a[3]);
            case 5:
                return python::make_tuple(a[0], a[1], a[2], a[3], a[4]);
            case 6:
                return python::make_tuple(a[0], a[1], a[2], a[3], a[4], a[5]);
            case 7:
                return python::make_tuple(a[0], a[1], a[2], a[3], a[4], a[5], a[6]);
            case 8:
                return python::make_tuple(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
        }
        throw std::invalid_argument("this");
    };
    np::dtype dt = np::dtype::get_builtin<T>();
    python::tuple shape = make_tuple_from_array(s);
    python::tuple strides = make_tuple_from_array(d);
    return np::from_data(This.origin(), dt, shape, strides, boost::python::object());
}


// C++ to NumPy
template<class T, size_t N>
struct multiarray_to_NumPy{
		static PyObject* convert(const multi_array<T,N>& v){
		return boost::python::incref(get<T,N>(v).copy().ptr());
	}
};

#define MULTIARRAY_CONV(type, N, name)  boost::python::to_python_converter<multi_array<type, N>, multiarray_to_NumPy<type, N> >();

BOOST_PYTHON_MODULE(_customConverters)
{
    using boost::python::def;
    np::initialize();
//*
    MULTIARRAY_CONV(bool, 1,"shared_bool_vector");
    MULTIARRAY_CONV(bool, 2,"shared_bool_matrix");
    MULTIARRAY_CONV(bool, 3,"shared_bool_tensor");
    MULTIARRAY_CONV(bool, 4,"shared_bool_tensor4");
    MULTIARRAY_CONV(bool, 5,"shared_bool_tensor5");
    MULTIARRAY_CONV(bool, 6,"shared_bool_tensor6");
    MULTIARRAY_CONV(bool, 7,"shared_bool_tensor7");
    MULTIARRAY_CONV(bool, 8,"shared_bool_tensor8");
    MULTIARRAY_CONV(uint8_t, 1,"shared_uint8_vector");
    MULTIARRAY_CONV(uint8_t, 2,"shared_uint8_matrix");
    MULTIARRAY_CONV(uint8_t, 3,"shared_uint8_tensor");
    MULTIARRAY_CONV(uint8_t, 4,"shared_uint8_tensor4");
    MULTIARRAY_CONV(uint8_t, 5,"shared_uint8_tensor5");
    MULTIARRAY_CONV(uint8_t, 6,"shared_uint8_tensor6");
    MULTIARRAY_CONV(uint8_t, 7,"shared_uint8_tensor7");
    MULTIARRAY_CONV(uint8_t, 8,"shared_uint8_tensor8");
    MULTIARRAY_CONV(uint16_t, 1,"shared_uint16_vector");
    MULTIARRAY_CONV(uint16_t, 2,"shared_uint16_matrix");
    MULTIARRAY_CONV(uint16_t, 3,"shared_uint16_tensor");
    MULTIARRAY_CONV(uint16_t, 4,"shared_uint16_tensor4");
    MULTIARRAY_CONV(uint16_t, 5,"shared_uint16_tensor5");
    MULTIARRAY_CONV(uint16_t, 6,"shared_uint16_tensor6");
    MULTIARRAY_CONV(uint16_t, 7,"shared_uint16_tensor7");
    MULTIARRAY_CONV(uint16_t, 8,"shared_uint16_tensor8");
    MULTIARRAY_CONV(uint32_t, 1,"shared_uint32_vector");
    MULTIARRAY_CONV(uint32_t, 2,"shared_uint32_matrix");
    MULTIARRAY_CONV(uint32_t, 3,"shared_uint32_tensor");
    MULTIARRAY_CONV(uint32_t, 4,"shared_uint32_tensor4");
    MULTIARRAY_CONV(uint32_t, 5,"shared_uint32_tensor5");
    MULTIARRAY_CONV(uint32_t, 6,"shared_uint32_tensor6");
    MULTIARRAY_CONV(uint32_t, 7,"shared_uint32_tensor7");
    MULTIARRAY_CONV(uint32_t, 8,"shared_uint32_tensor8");
    MULTIARRAY_CONV(uint64_t, 1,"shared_uint64_vector");
    MULTIARRAY_CONV(uint64_t, 2,"shared_uint64_matrix");
    MULTIARRAY_CONV(uint64_t, 3,"shared_uint64_tensor");
    MULTIARRAY_CONV(uint64_t, 4,"shared_uint64_tensor4");
    MULTIARRAY_CONV(uint64_t, 5,"shared_uint64_tensor5");
    MULTIARRAY_CONV(uint64_t, 6,"shared_uint64_tensor6");
    MULTIARRAY_CONV(uint64_t, 7,"shared_uint64_tensor7");
    MULTIARRAY_CONV(uint64_t, 8,"shared_uint64_tensor8");
    MULTIARRAY_CONV(int8_t, 1,"shared_int8_vector");
    MULTIARRAY_CONV(int8_t, 2,"shared_int8_matrix");
    MULTIARRAY_CONV(int8_t, 3,"shared_int8_tensor");
    MULTIARRAY_CONV(int8_t, 4,"shared_int8_tensor4");
    MULTIARRAY_CONV(int8_t, 5,"shared_int8_tensor5");
    MULTIARRAY_CONV(int8_t, 6,"shared_int8_tensor6");
    MULTIARRAY_CONV(int8_t, 7,"shared_int8_tensor7");
    MULTIARRAY_CONV(int8_t, 8,"shared_int8_tensor8");
    MULTIARRAY_CONV(int16_t, 1,"shared_int16_vector");
    MULTIARRAY_CONV(int16_t, 2,"shared_int16_matrix");
    MULTIARRAY_CONV(int16_t, 3,"shared_int16_tensor");
    MULTIARRAY_CONV(int16_t, 4,"shared_int16_tensor4");
    MULTIARRAY_CONV(int16_t, 5,"shared_int16_tensor5");
    MULTIARRAY_CONV(int16_t, 6,"shared_int16_tensor6");
    MULTIARRAY_CONV(int16_t, 7,"shared_int16_tensor7");
    MULTIARRAY_CONV(int16_t, 8,"shared_int16_tensor8");
    MULTIARRAY_CONV(int32_t, 1,"shared_int32_vector");
    MULTIARRAY_CONV(int32_t, 2,"shared_int32_matrix");
    MULTIARRAY_CONV(int32_t, 3,"shared_int32_tensor");
    MULTIARRAY_CONV(int32_t, 4,"shared_int32_tensor4");
    MULTIARRAY_CONV(int32_t, 5,"shared_int32_tensor5");
    MULTIARRAY_CONV(int32_t, 6,"shared_int32_tensor6");
    MULTIARRAY_CONV(int32_t, 7,"shared_int32_tensor7");
    MULTIARRAY_CONV(int32_t, 8,"shared_int32_tensor8");
    MULTIARRAY_CONV(int64_t, 1,"shared_int64_vector");
    MULTIARRAY_CONV(int64_t, 2,"shared_int64_matrix");
    MULTIARRAY_CONV(int64_t, 3,"shared_int64_tensor");
    MULTIARRAY_CONV(int64_t, 4,"shared_int64_tensor4");
    MULTIARRAY_CONV(int64_t, 5,"shared_int64_tensor5");
    MULTIARRAY_CONV(int64_t, 6,"shared_int64_tensor6");
    MULTIARRAY_CONV(int64_t, 7,"shared_int64_tensor7");
    MULTIARRAY_CONV(int64_t, 8,"shared_int64_tensor8");
    MULTIARRAY_CONV(float, 1,"shared_float_vector");
    MULTIARRAY_CONV(float, 2,"shared_float_matrix");
    MULTIARRAY_CONV(float, 3,"shared_float_tensor");
    MULTIARRAY_CONV(float, 4,"shared_float_tensor4");
    MULTIARRAY_CONV(float, 5,"shared_float_tensor5");
    MULTIARRAY_CONV(float, 6,"shared_float_tensor6");
    MULTIARRAY_CONV(float, 7,"shared_float_tensor7");
    MULTIARRAY_CONV(float, 8,"shared_float_tensor8");
    MULTIARRAY_CONV(double, 1,"shared_double_vector");
    MULTIARRAY_CONV(double, 2,"shared_double_matrix");
    MULTIARRAY_CONV(double, 3,"shared_double_tensor");
    MULTIARRAY_CONV(double, 4,"shared_double_tensor4");
    MULTIARRAY_CONV(double, 5,"shared_double_tensor5");
    MULTIARRAY_CONV(double, 6,"shared_double_tensor6");
    MULTIARRAY_CONV(double, 7,"shared_double_tensor7");
    MULTIARRAY_CONV(double, 8,"shared_double_tensor8");
    //*/
    
    
	VECTOR_SEQ_CONV(int);
	VECTOR_SEQ_CONV(double);
	VECTOR_SEQ_CONV(std::string);
    
}
