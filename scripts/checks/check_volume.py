# Check volume matrix consistency

import VtkReader as vtk
import math

r = vtk.Reader('spheres.0.vtu');
r.load();

V1 = r.volumeMatrix(-1,1,1000);
V2 = r.volumeMatrix(-1,1,1);

rr = r.pointData('radii');

V = 4./3.*math.pi*rr**3;

# Check total volume of each sphere
print(sum(sum(abs(V.transpose() - sum(V1)) < 1e-10)) == len(rr))
print(sum(sum(abs(V.transpose() - sum(V2)) < 1e-10)) == len(rr))


