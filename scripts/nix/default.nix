with (import <nixpkgs> {});                                                                  

stdenv.mkDerivation rec {

      name = "vtkreader-env";

      buildInputs = [
	boost
	python37Full
        python37Packages.numpy
	makeWrapper
        python3Packages.wrapPython	
        cmake
        vtk
     ] ++ (with python37Packages; [
                        numpy ipython ipython_genutils boost future
                      ]) ;
#    ignoreCollisions=true;
    }
