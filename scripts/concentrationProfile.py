#!/usr/bin/ipython
# Coding: utf-8
# (c) 2019 William Chèvremont <william.chevremont@univ-grenoble-alpes.fr>

import argparse, os, sys, glob
import numpy as np
import math
import re
import csv
from VtkReader import *

#######################################
# Arguments parsing
#######################################

prog = os.path.basename(sys.argv[0])
parser=argparse.ArgumentParser(usage='%s [options] <vtkDir> <outputFile>'%prog, description='%s compute profiles from vtk data.'%prog)
parser.add_argument('-d','--dir',dest='d', type=str, help='Direction on which the profile is computed. [x,y,z]', default='y')
parser.add_argument('--what',dest='what', type=str, metavar='LIST', help='What profiles should I compute?', default='concentration')
parser.add_argument('--from', dest='start', type=float, help='Minimum coordinate on which the profile is computed. Auto-computed by default.')
parser.add_argument('--to', dest='to', type=float, help='Maximum coordinate on which the profile is computed. Auto-computed by default.')
parser.add_argument('--mask', dest='mask', type=int, help='Restrict to data with mask corresponding to.', default=0)
parser.add_argument('-N', dest='N', type=int, help='Number of slices to consider.', default=20)
#parser.add_argument('--outside', dest='outside', action='store_true', help='Also compute the values outside the bonds.')
parser.add_argument('-A', dest='A', type=float, help='Area on which the slice is reported. If not provided, the concentration will be the total volume of spheres inside the slice.', default=1)
#parser.add_argument('-e','--enlarge-factor', dest='e', type=float, help="Enlarge factor. This allows to cross slices and smooth the profile", default=1.)
parser.add_argument('vtkDir',type=str, help='VTK source dir')
parser.add_argument('outputFile',type=str, help='output file')
opts = parser.parse_args();


if not os.path.exists(opts.vtkDir) or os.path.isfile(opts.vtkDir):
    print("vtkDir should exists and contain spheres.*.vtu files");
    sys.exit(-1);
    
if opts.start is None or opts.to is None:
    print("From and to must be provided!");
    sys.exit(-1);
    
xyz = -1;

if opts.d == 'x': xyz = 0;
if opts.d == 'y': xyz = 1;
if opts.d == 'z': xyz = 2;

if xyz == -1:
    print("direction should be x, y or z");
    sys.exit(-1);

files = [ f for f in glob.glob(opts.vtkDir + '/spheres.*.vtu')];

if len(files) == 0:
    print("No files to analyse")
    sys.exit(-1);
    
regfile = '(?:.*)spheres\.([0-9]+)\.vtu$';
    
files = sorted(files, key=lambda f: int(re.match(regfile,f).group(1)))
titles = [];
what = opts.what.split(',')
            
#######################################
# Extract datas
#######################################

dx = (opts.to - opts.start)/opts.N;
profile_pos = [dx*(i+0.5) + opts.start for i in range(0, opts.N)]

did = -1;

for f in files:    
    
    # Read file
    reader = Reader(f);
    reader.load();
    
    # Deduce titles at first file.
    if did == -1:
        for w in what:
            
            if w == 'concentration':
                titles += ['phi'];
                continue;
            
            n = reader.pointData(w).shape[1]
            
            if n == 1:
                titles += [w];
            elif n == 3:
                titles += [w+'_x', w+'_y', w+'_z'];
            elif n == 9:
                titles += [w+'_xx', w+'_xy', w+'_xz', w+'_yx', w+'_yy', w+'_yz', w+'_zx', w+'_zy', w+'_zz'];
            else:
                for i in range(0,n):
                    titles += [w+'_'+str(i)];
        eDatas = np.zeros((len(files), len(titles), opts.N))
    
    did += 1
    
    # Compute volume repartition matrix
    VM = reader.volumeMatrix(opts.start, opts.to, opts.N, mask=opts.mask, dir=xyz);
    
    wid = 0;
    for w in what:
        if w == 'concentration':
            eDatas[did, wid, :] = VM.dot(np.ones(VM.shape[1]))/(dx*opts.A);
            wid += 1;
        else:
            cmpt = reader.pointData(w);
            if cmpt.shape[1] == 3:
                eDatas[did, wid:(wid + cmpt.shape[1]), :] = VM.dot(cmpt).transpose()/(VM.dot(np.ones(VM.shape[1]))); # divide by sphere volume vectors
            else:
                eDatas[did, wid:(wid + cmpt.shape[1]), :] = VM.dot(cmpt).transpose()/(dx*opts.A); # Divide others (tensors) by slice volume
            wid += cmpt.shape[1];
        
#######################################
# Save to file
#######################################

iterid = [re.match(regfile, f).group(1) for f in files];
tt = ['# pos'] +  [t+'.'+iid for t in titles for iid in iterid];

with open(opts.outputFile, 'w') as cfile:
    fout = csv.writer(cfile, delimiter='\t');
    
    fout.writerow(tt); # Write header
    
    for x in range(0, len(profile_pos)):
        fout.writerow([profile_pos[x]] + [eDatas[iid,tid,x] for tid in range(0, len(titles)) for iid in range(0, len(iterid))]);

